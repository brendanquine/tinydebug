# tinyDebug #
---

Hello and welcome to my first Arduino community contribution (let's hope it doesn't suck)!

Have you ever been working on a project that you know you'll be [shrinkifying](https://youtu.be/30rPt802n1k) later, dreading the fact that you'll have to go back through all of your nice serial debugging comments and cut them all out for size? Well, now you don't have to!

### Info ###

* Ver 1.0
* Allows for easy addition/removal of debug comments from compilation!

*By changing a single value, this library tells the compiler to ignore all of your debugging comments entirely, freeing up the memory they would have otherwise occupied.*

### Installation ###

<-- Click the [download link](https://bitbucket.org/brendanquine/tinydebug/downloads) to grab a zip of the full project.

* Unzip the folder into your sketchbook's *libraries* folder, and make sure it's named *tinyDebug*.
* If the Arduino IDE was running, restart it. tinyDebug should now show up in your list of libraries.
* Load the example sketch, *tinyDebugDemo* and read the instructions
* That's it!

### Usage ###

There are only two "functions" in this library. I phrase it this way because these "functions" are actually cleverly disguised pre-processor #define directives!
~~~~
dOn(int n);
~~~~
A shortcut to **Serial**.begin() on your Arduino, with a twist:

* If you pass it a "0", it won't appear in your compiled code at all, and neither will any of your debug messages!
* If you pass it a "1", it'll use 9600 as its baud rate.
* You can also pass it any other baud rate manually. example: **dOn**(115200);

~~~~
d("message");
~~~~
Stores all strings in flash, and **Serial**.print()s them on new lines automatically.

* To use it, just call d("message"); and it'll do the rest.

When you're ready for permanent install of your creation, use **dOn**(0); and all messages will be ignored by the compiler! That's all there is to it! If you see something that needs fixing, send me an email at brendanquine@gmail.com, or PM me on reddit at /u/brendanquine

Take care!


### About the Author ###

* Name: Brendan Quine
* Email: BrendanQuine@gmail.com
* Reddit: [/u/brendanquine](https://www.reddit.com/u/BrendanQuine)