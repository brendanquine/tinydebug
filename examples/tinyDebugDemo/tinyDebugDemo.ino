/*************************************************
 *  Sketch: tinyDebug Demo
 *  Purpose: Allows for easy addition/removal of debug comments from compilation!
 *  Author:  Brendan Quine
 *  Date:    2016-02-24
 *
 *  MIT License
 ************************************************
 * To see how cool this is, compile this code twice.
 * 
 * First, compile it with dOn(9600); ((or whatever baudrate you like!))
 * See how big the file is?
 * 
 * Alright, now use dOn(0); and recompile.
 * Now, all of the debugging code is completely ignored!
 ************************************************/

#include <tinyDebug.h>

void setup() {
  dOn(9600);
  delay(2000);
  d("It's time to test the tinyDebug library!");
  d("");
  delay(2000);
  d("This is a debug message!");
  delay(500);
  d("So is this!");
  delay(500);
  d("Uh-oh, looks like something went wrong here.");
  delay(500);
  d("Another generic debug message.");
  d("");
  delay(2000);
  d("If used intelligently,");
  delay(1000);
  d("these messages can help pinpoint your bugs.");
  delay(1000);
  d("..But hey, you never know!");
  delay(2000);
  d("Maybe you coded everything right,");
  delay(1000);
  d("and the code is just wrong somewhere deeper...");
  delay(500);
  d("...way down in those ones and zeros..");
  d("");
  delay(2000);
  delay(2000);
  d("Eh, probably not.");
  delay(2000);
  d("...it was probably your mistake.");
  delay(2000);
  d("Ok, good talk.");
  delay(2000);
  d("Bye!");
  d(":p");
}

void loop() {}