/*************************************************
 *  Library: tinyDebug
 *  Purpose: Allows for easy addition/removal of debug comments from compilation!
 *  Author:  Brendan Quine
 *  Date:    2016-02-24
 *  Version: 1.0
 *
 *  MIT License
 ************************************************/

#ifndef tinyDebugLib_h
#define tinyDebugLib_h

#if (ARDUINO >= 100)
#include <Arduino.h>
#else
#include <WProgram.h>
#endif

bool tinyDebugLib = 0;  //this is the only thing that will always take up memory.

#define dOn(n) do{if(n==0){return;}else if(n==1){tinyDebugLib=n;if(tinyDebugLib){Serial.begin(9600);}}else{tinyDebugLib=1;if(tinyDebugLib){Serial.begin(n);}}} while(0)
/* A shortcut to Serial.begin() on your arduino, with a twist:
 * If you pass it a "0", it won't appear in your compiled code at all,
 * and neither will any of your debug messages!
 * If you pass it a "1", it'll use 9600 as its baud rate.
 * You can also pass it any other baud rate manually. ex: dOn(115200);
 ************************************************/

#define d(m) do{if(tinyDebugLib){Serial.println(F(m));}} while(0)
/* This awesome. It stores all strings in flash,
 * and prints them on new lines automatically.
 * To use it, just call d("message"); and it'll do the rest.
 * 
 * When you're ready for permanent install of your creation,
 * use dOn(0); and all messages will be ignored!
 ************************************************/
#endif